const Apify = require('apify')
const {
    utils: { log },
} = Apify

exports.logInfo = (message, data = {}) => {
    log.info(message, data)
}

exports.logUrlInfo = (message, context, data = {}) => {
    log.info(message, { url: context.request.url, ...data })
}

exports.logError = (message, error = {}) => {
    log.error(message, { error: error.message })
}

exports.logUrlError = (message, context, error = {}) => {
    log.error(message, { url: context.request.url, error: error.message || '' })
}
