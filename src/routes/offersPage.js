const logger = require('../utils/logger')

//
// MAIN
//

exports.label = 'OFFERS'

exports.handlePage = async (context, dataset, keyword) => {
    logger.logUrlInfo('Scraping offers page.', context)

    const product = context.request.userData.product

    const offersData = await context.page.$$eval('.olpOffer', (elements) =>
        elements.map((element) => {
            return {
                sellerName: element.querySelector('h3') ? element.querySelector('h3').innerText : null,
                price: element.querySelector('.olpOfferPrice')
                    ? element.querySelector('.olpOfferPrice').innerText
                    : null,
                shippingPrice: null,
            }
        })
    )

    for (const offerData of offersData) {
        await dataset.pushData({
            title: product.title,
            url: product.url,
            description: product.description,
            keyword,
            ...offerData,
        })
    }
}
