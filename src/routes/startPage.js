const logger = require('../utils/logger')
const detailPage = require('../routes/detailPage')

//
// MAIN
//

exports.handlePage = async (context, requestQueue) => {
    logger.logUrlInfo('Scraping start page.', context)

    await context.page.waitForSelector('.s-search-results')

    const products = await context.page.$$eval('div[data-asin]', (elements) =>
        elements
            .map((element) => {
                return {
                    asin: element.getAttribute('data-asin'),
                }
            })
            .filter((product) => product.asin && product.asin.length > 0)
    )

    for (const product of products) {
        await requestQueue.addRequest({
            url: `https://www.amazon.com/dp/${product.asin}`,
            userData: {
                asin: product.asin,
                label: detailPage.label,
            },
        })
    }
}
