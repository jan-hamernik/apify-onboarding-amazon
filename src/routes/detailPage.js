const logger = require('../utils/logger')
const offersPage = require('../routes/offersPage')

//
// MAIN
//

exports.label = 'DETAIL'

exports.handlePage = async (context, requestQueue) => {
    logger.logUrlInfo('Scraping detail page.', context)

    const asin = context.request.userData.asin

    const title = await getSelectorContent(context, `head > meta[name='title']`)
    const description = await getSelectorContent(context, `head > meta[name='description']`)

    await requestQueue.addRequest({
        url: `https://www.amazon.com/gp/offer-listing/${asin}`,
        userData: {
            label: offersPage.label,
            product: {
                url: context.request.url,
                asin,
                title,
                description,
            },
        },
    })
}

const getSelectorContent = async (context, selector) => {
    try {
        await context.page.waitForSelector(selector)
        let string = await context.page.$eval(selector, (element) => element.content.trim())
        return string.startsWith('Amazon.com: ') ? string.replace('Amazon.com: ', '') : string
    } catch (error) {
        logger.logUrlError(`${selector} not found.`, context, error)
    }
    return null
}
