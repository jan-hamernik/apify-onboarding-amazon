const Apify = require('apify')
const uuid = require('uuid')

const logger = require('./utils/logger')

const startPage = require('./routes/startPage')
const detailPage = require('./routes/detailPage')
const offersPage = require('./routes/offersPage')

Apify.main(async () => {
    const { keyword } = await Apify.getInput()
    const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`

    const requestList = await Apify.openRequestList('start-urls', [startUrl])
    const requestQueue = await Apify.openRequestQueue()
    const proxyConfiguration = await Apify.createProxyConfiguration()

    const dataset = await Apify.openDataset()

    const runData = {
        errorCount: 0,
    }

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        proxyConfiguration,
        useSessionPool: true,
        persistCookiesPerSession: true,
        launchPuppeteerOptions: {
            useChrome: true,
            stealth: true,
            headless: true,
        },
        handlePageFunction: async (context) => {
            const htmlSource = await context.page.evaluate(() => document.documentElement.outerHTML)

            if (htmlSource.toLowerCase().includes('something went wrong on our end')) {
                if (runData.errorCount >= 3) {
                    logger.logUrlInfo('Page not loaded: retire session.', context)
                    await context.session.retire()
                    runData.errorCount = 0
                }

                logger.logUrlInfo('Page not loaded: enqueue request.', context, runData)
                await requestQueue.addRequest({
                    uniqueKey: `${context.request.url}-${new Date().toLocaleString()}-${uuid.v4()}`,
                    url: context.request.url,
                    userData: context.request.userData,
                })

                runData.errorCount += 1
                return
            }

            switch (context.request.userData.label) {
                case detailPage.label:
                    return detailPage.handlePage(context, requestQueue)
                case offersPage.label:
                    return offersPage.handlePage(context, dataset, keyword)
                default:
                    return startPage.handlePage(context, requestQueue)
            }
        },
    })

    logger.logInfo('Starting the crawl.')
    await crawler.run()
    logger.logInfo('Crawl finished.')
})
